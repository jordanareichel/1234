<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="PT-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

        <div id="nav">
                    <div id="nav_wrapper">

                        <ul>
                    
                            <li><a href="">SCC</a></li><li>
                            <li><a href="">Contatos</a></li><li>    
                            <a href="">Administração ▼</a>
                                <ul>
                                    <li><a href="<?= Url::base().'/administrador'?>">Proprietarios</a></li>
                                    <li><a href="<?= Url::base().'/providencias'?>">Controle de Aluguel</a></li>
                                    <li><a href="<?= Url::base().'/requerimentos'?>">Moradores</a></li>
                                  
                                </ul>
                            </li>
                            <li><a href="">Login</a></li><li>
                                
                        </ul>   
                        
                    </div>
                </div>




    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
