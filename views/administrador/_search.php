<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdministradorSearch */
/* @var $form yii\widgets\ActiveForm */
$std = \app\models\Administrador::adm();

?>

<div class="administrador-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    

    <?php // $form->field($model, 'adm_id') ?>

    <?php // $form->field($model, 'adm_proprietario') ?>

    <?php //$form->field($model, 'adm_condominio') ?>

    <?php // $form->field($model, 'adm_lote') ?>

    <div class="form-group">
        <?php //Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?php // Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
