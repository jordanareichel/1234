<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdministradorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Administrador';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="administrador-index">


    <h1><?= Html::encode($this->title) ?></h1>


    <p>
        <?= Html::a ('Novo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    </div>


<?= $this->render('_search', ['model' => $searchModel]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
    
        'tableOptions'=>['class'=>'table table-condensed'],   

        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            #'adm_id',
            'adm_proprietario',
            'adm_condominio',
            'adm_lote',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
