<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Administrador */

$this->title = 'Novo Proprietario';
?>
<div class="administrador-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
