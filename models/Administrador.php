<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "administrador".
 *
 * @property integer $adm_id
 * @property string $adm_proprietario
 * @property integer $adm_condominio
 * @property string $adm_lote
 */
class Administrador extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'administrador';
    }
    public function afterFind()
    {
        if (!empty($this->name)) {

            if($this->name == 1 ){
                $this->name = '0-5';
            }

            if($this->name == 2 ){
                $this->name = '6-10';
            }

            if($this->name == 3 ){
                $this->name = '11-15';
            }

            if ($this->name == 4) {
                $this->name = '16-20';
            }
           
        }

        return parent::afterFind();
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adm_proprietario', 'adm_condominio', 'adm_lote'], 'required'],
            [['adm_condominio'], 'required'],
            [['adm_lote'], 'required'],
            [['adm_proprietario'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'adm_id' => 'ID',
            'adm_proprietario' => 'Proprietario',
            'adm_condominio' => 'Condominio',
            'adm_lote' => 'Lote',
        ];
    }

    public function adm () {

        $adm = [
                '1' => '0-5',
                '2' => '6-10',
                '3' => '11-15',
                '4' => '16-20',
        ];

        return $adm;
    }

    public function cdm () {

        $cdm = [

                '1' => 'A',
                '2' => 'B',
                '3' => 'C',
                '4' => 'D',
                '5' => 'E',

        ];
            return $cdm;
    }
}
